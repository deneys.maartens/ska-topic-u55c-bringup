#! /bin/sh -ex
#
# examine U55C card

DATE=$(date --iso=sec)

SN=$(sudo /opt/xilinx/xrt/bin/xbmgmt examine --device 0000:01:00.0 |
        grep Serial |
        cut -d: -f 2 |
        tr -d ' ')
echo $SN

mkdir $SN.$DATE
mv prog-* lspci-* $SN.$DATE/
cd $SN.$DATE

sudo lspci -vd 10ee: |
  tee lspci

xbutil examine |
  tee examine

xbutil examine --device 0000:01:00.1 |
  tee info-user

sudo /opt/xilinx/xrt/bin/xbmgmt examine --device 0000:01:00.0 |
  tee info-mgmt

xbutil --batch validate -d 0000:01:00.1 |
  tee validate

xbutil --batch --verbose validate -d 0000:01:00.1 |
  tee validate-verbose

cd -

# -fin-
