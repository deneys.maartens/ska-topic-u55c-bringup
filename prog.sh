#! /bin/sh -e
#
# program U55C

DATE=$(date --iso=sec)
sudo lspci -vd 10ee: |
  tee lspci-$DATE

echo

sudo /opt/xilinx/xrt/bin/xbmgmt program --base --device 0000:01:00.0 --image xilinx_u55c_gen3x16_xdma_base_3 |
  tee prog-$DATE

# -fin-
