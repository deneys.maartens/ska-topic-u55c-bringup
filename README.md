U55c bringup scripts
====================

The procedure used to bring up the accelerator cards:

- download and install the Xilinx software:
  https://www.xilinx.com/products/boards-and-kits/alveo/u55c.html#gettingStarted
- install the card into your machine
- run `./prog.sh`; this creates two files, namely
  - `lspci-<datestamp>` -- output of `lspci -vd 10ee:`
  - `prog-<datestamp>` -- output of `xbmgmt program ...`
  if the flash process completes successfully, it should request a cold restart
- do a cold restart of the machine; if the card isn't detected, don't panic;
  follow the cold restart with a warm restart and it _should_ show up
- run `./examine.sh`
  - this creates a directory called `<serial-number>-<datestamp>`
  - it moves any `lspci-*` and `prog-*` files into this newly created directory;
    these files needs to exit otherwise the script will error out
  - it then goes on to create a number of files in said directory
    - `examine` -- output of `xbutil examine`
    - `info-mgmt` -- output of `xbmgmt examine --device 0000:01:00.0`
    - `info-user` -- output of `xbutil examine --device 0000:01:00.1`
    - `lspci` -- output of `lspci -vd 10ee:`
    - `validate` -- output of `xbutil validate -d 0000:01:00.1`
    - `validate-verbose` -- output of `xbutil --verbose validate -d 0000:01:00.1`

Note, these scripts assume the PCI addresses of the card are 0000:01:00.{0,1};
modify as appropriate
